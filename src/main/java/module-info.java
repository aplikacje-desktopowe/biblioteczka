module com.example.biblioteczka {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;

    opens com.example.biblioteczka to javafx.fxml;
    exports com.example.biblioteczka;
}